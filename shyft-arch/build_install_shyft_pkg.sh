#!/bin/bash
tmpdir=$(mktemp -d)
chmod ugo+rwx ${tmpdir}
pushd ${tmpdir}
git clone --depth 1 --single-branch --branch master  https://gitlab.com/shyft-os/shyft.git
mkdir -p shyft/buildr
pushd shyft/buildr
cmake .. -G Ninja -DSHYFT_WITH_TESTS=0
ninja install && cd ../python && (sudo python setup.py install)
popd
popd
sudo rm -rf ${tmpdir}
