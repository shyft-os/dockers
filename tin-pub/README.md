# Docker For the Tin Model publication 

This repository contains all scripts and files needed to create the Docker  image that contains
the source code and data-files for the Tin model publication by Olga Sylantieva at UIO:

``
     **Shyft and Rasputin**: a toolbox for hydrologic simulations on
triangular irregular meshes

Olga Silantyeva 1 , Ola Skavhaug 2 , Bikas C. Bhattarai 1 , John F. Burkhart 1,3 , Sigbjørn Helset 3 , and
Magne Nordaas 2

1
Universitet i Oslo, Oslo, Norway
Expert Analytics AS, Oslo, Norway
3
Statkraft AS, Oslo, Norway
2

Correspondence: Olga Silantyeva (olga.silantyeva@geo.uio.no)

``

This image is built using the Shyft developer image:
 
registry.gitlab.com/shyft-os/dockers/arch-dev,

as the starting point.

Then build shyft into the /tmp, and then pip install it into the image.

## Dependencies 

The base image
 
registry.gitlab.com/shyft-os/dockers/arch-dev,

and it's dependencies if you would like to build everything from scratch.

We guess you have ~ 24..32 GB Ram and 4+ cores to work with.

## Usage
Run `sudo make docker-image` to build the base image

## Purpose
* Provide docker with prebuilt and installed Shyft ready to use
* Easy to further develop and build Shyft (it comes with all that's in arch-dev image)

## Use of the Shyft developer image
The image contains all recent c++ compilers, debuggers, and complete set of python modules.
SHYFT_DEPENDENCIES_DIR is unset, but all packages are found
on the system.

## Example usage

The docker contains all needed packages for using Shyft.

To verify everything is Ok, we can checkout the shyft-data git repository, 
and then the shyft-repository containing the python test_suites.


```bash
sudo docker run -it -u ci -v $HOME/projects:/home/ci/projects registry.gitlab.com/shyft-os/dockers/tin-pub:latest bash
```

Then cd /home/ci/projects (its mapped to $HOME/projects, so the checkout part could already be done)

```bash
# inside the docker image as started above
cd /home/ci/projects
git clone https://gitlab.com/shyft-os/shyft.git
mv shyft/shyft shyft/shyft.x # move away the checked out shyft-python packages, we are using the installed ones!
git clone https://gitlab.com/shyft-os/shyft-data.git
export SHYFT_DATA=/home/ci/projects/shyft-data # ensure to point test-suite to data-directory used by tests.
cd shyft
pytest --workers 4 --cov=shyft test_suites
# or  as some of the most up-to-date fancy features might be not covered yet, to test only hydrology part use:
pytest --workers 4 --cov=shyft test_suites/hydrology
```

## To build the docker image

Please refer to the https://www.gitlab.com/shyft-os/dockers, directory arch-dev,

https://gitlab.com/shyft-os/dockers/blob/master/arch-dev/README.md

 
If you have a decent setup, its time to work on this docker that is a mod of the archlinux one.

* git clone shyft-os/dockers
* sudo make docker-image
* then maybe update build_install_shyft_pkg.sh or the Dockerfile
* test the docker image, building and testing shyft with it, e.g. as explained above

## To publish a new version

Main help/recipe is here:
 
 https://docs.gitlab.com/ee/user/packages/container_registry


Given that you are member with needed access in gitlab.com/shyft-os, 
you generate an api-token to use for the login (because we are 2FA)
Then login using the token as password, e.g.
```bash
 docker login -u <your-user@gitlab.com> --password-stdin registry.gitlab.com/shyft-os/dockers
```
* then push
```bash
sudo docker push registry.gitlab.com/shyft-os/dockers/tin-pub
```
