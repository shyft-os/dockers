#!/bin/bash
export CMAKE_BUILD_PARALLEL_LEVEL=4
tmpdir=$(mktemp -d)
chmod ugo+rwx ${tmpdir}
pushd ${tmpdir}
git clone --branch v4.9.1 --depth 1 https://gitlab.com/shyft-os/shyft.git
mkdir -p shyft/build
pushd shyft/build
cmake .. -G Ninja
ninja -j 8 install
cd ..
sudo python setup.py install
popd
popd
sudo rm -rf ${tmpdir}
