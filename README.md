# Dockers for Shyft

This repository contains docker build recipes for the Shyft
eco-system.

## Build dockers

Contains needed components to compile and run c++ tests.
We also need tools to produce code-quality reports, like coverage, valgrind etc.

Since the python surface to the 3rd party components, it might be ok to
have separate test-dockers for python parts.

## Python test dockers

For running python tests, these dockers do not need to have 
c++ compilers etc. as the build -dockers

## Development dockers

Contains all needed tools to do efficient full-stack 
development with shyft. This includes compilers, git, code-quality tools, 
as well python-packages used by Shyft.

## Application dockers

For running Shyft (python) applications, based on 
published versions of shyft.

This could be like  dtss-docker, drms-docker, dstm-docker with services,
or it could be dashboard-docker with a ready made presentation front end.

