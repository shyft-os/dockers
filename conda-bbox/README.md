# conda-bbox

This container is used to provide a build-box for the Shyft conda/conda-pip based packages that 
are uploaded to the  configured anaconda server. The default is anaconda.org.

It replaces the initial strategy using virtual-box images or similar to do the build work,
since it's easier to distribute/arrange containers than handcrafted virtualbox images.

**Tip!** If you have an internal build scenario, you can mirror the Shyft repository on gitlab,
and then set up your own private gitlab-runners, providing your own customized conda-bbox images.

**oracle linux** provides free of charge high quality linux distro that allows building Shyft against 
older c-libraries and linux-kernels (like redhat). 
Most important, it provides an updated c++ compiler required to compile Shyft and dependencies.

The following simple rules are applied:

- Official Oracle linux provided container images and packages
- Build  from  tagged/versioned sources those packages that are not available
- Use anaconda/python distro system by default
- Use selected anaconda/conda-forge packages where not yet available on anaconda.org 

# How it is intended to be used

The shyft-os/shyft linux pipeline will require a conda-bbox:latest image to build the linux conda-packages.

You can also use it in a developer setup, if you would like to work with anaconda-style of packages. 

For ordinary developer-setup with containers we strongly recommend using the arch-linux based containers.

# Content overview

For details, look into the conda-bbox.Dockerfile, build_deps.sh.

- oraclelinux:8-slim base image (to align with RHEL/OL linux based corporate).

- gcc-toolset-12, cmake etc. 
   
- leveldb (from source)

- git
  
- lapack (needed for shyft_dependencies armadillo etc.)

- /projects/shyft_dependencies (with built shyft deps, boost, arma, dlib, )

- /root/.bashrc  (with proper source lines to select devtoolset conda shyft_dep_dir)

- /projects/miniconda  (with envs according to build_deps.sh script)

## Sintef Shop Api

If you would like to create a conda-bbox with the Sintef Shop Api, then
you could achieve this by adjusting the build_deps.sh recipe, 
and install the required components of Sintef Shop Api into the conda-bbox image.
The resulting build of Shyft would then be with the integrated shop algorithm functionality.

## conda environments

The shyft_xx environments are used for building/testing/publishing open-source Shyft.

Currently, we are able to create a working 3.8 env using mostly anaconda/main packages
and a few conda-forge.

The 3.10 environment currently require more conda-forge packages since 
working permutations of the packages from anaconda/main is not yet available.

If you are looking for stable, high quality python system, we recommend using 
distro based systems.

Shyft is maintained and developed using Arch Linux.
It is known to work well on Fedora, and recent versions of Ubuntu.
