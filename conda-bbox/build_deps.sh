#!/bin/bash

set -o errexit # exit on failure set -e set -o nounset # exit on undeclared vars set -u
set -o pipefail # exit status of the last command that threw non-zero exit code returned
# Debugging set -x
# set -o xtrace

export SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=/projects}
build_support_dir=$(readlink --canonicalize --no-newline `dirname ${0}`)
# to align the cmake support:
SHYFT_DEPENDENCIES_DIR=${SHYFT_DEPENDENCIES_DIR:=${SHYFT_WORKSPACE}/shyft_dependencies}
armadillo_name=armadillo-12.6.4
dlib_ver=19.24
dlib_name="dlib-${dlib_ver}"
boost_ver=1_83_0
pybind11_ver=v2.4.3
miniconda_ver=latest
libfmt_ver=10.1.0
libfmt_name="fmt-${libfmt_ver}"
rocksdb_ver=8.5.3

cmake_common="-DCMAKE_INSTALL_MESSAGE=NEVER"
echo ---------------
echo Update/build shyft dependencies
echo SHYFT_WORKSPACE........: ${SHYFT_WORKSPACE}
echo SHYFT_DEPENDENCIES_DIR.: ${SHYFT_DEPENDENCIES_DIR}
echo PACKAGES...............: miniconda ${miniconda_ver} w/shyft_env, doctest, boost_${boost_ver}, ${armadillo_name}, ${dlib_name} ${libfmt_name},otlv4,Howard Hinnant date
if [[ -f /opt/rh/gcc-toolset-13/enable ]] ; then
  echo Enable gcc toolset
  source /opt/rh/gcc-toolset-13/enable
else
  echo gcc toolset not found, assuming ok to use current compiler
fi

if ! type cmake >/dev/null 2>&1 ; then
  cmake_ver=3.27.4
  echo  build cmake ${cmake_ver}
  (cd /tmp && curl -L -O https://github.com/Kitware/CMake/releases/download/v${cmake_ver}/cmake-${cmake_ver}.tar.gz && \
    tar -xf cmake-${cmake_ver}.tar.gz && \
    cd cmake-${cmake_ver} && \
    ./bootstrap --parallel=20 -- -DCMAKE_BUILD_TYPE:STRING=Release && \
    make -j 20 >/dev/null && make install >/dev/null && \
    rm -rf /tmp/* || true
  )
else
  echo cmake found ${cmake --version}
fi
if ! type ccache >/dev/null 2>&1 ; then
  ccache_ver=v4.8.3
  echo build ccache ${ccache_ver}
  (cd /tmp && \
    git clone --depth 1 --branch ${ccache_ver} https://github.com/ccache/ccache && \
    pushd ccache && \
    cmake -B bld . -DCMAKE_BUILD_TYPE=Release -DHIREDIS_FROM_INTERNET=ON && \
    cmake --build bld --config Release -j 4 && \
    cmake --install bld --config Release && \
    popd && \
    rm -rf /tmp/* || true
  )
fi


# the current versions we are building
mkdir -p "${SHYFT_DEPENDENCIES_DIR}"
cd "${SHYFT_DEPENDENCIES_DIR}"
#
# INSTALL AND CONFIGURE ANACONDA
#
wget -q -O /tmp/miniconda.sh http://repo.continuum.io/miniconda/Miniconda3-${miniconda_ver}-Linux-x86_64.sh
bash /tmp/miniconda.sh -b -p "${SHYFT_WORKSPACE}"/miniconda
source "${SHYFT_WORKSPACE}"/miniconda/etc/profile.d/conda.sh
conda init bash
conda activate base
conda config --set always_yes yes --set changeps1 no
conda update -y conda
conda update -y --all
conda install -y anaconda-client

py_build_packs="conda-build  conda-verify setuptools anaconda-client pip twine 'numpy>1.20'"
py_dashboard_packs="bokeh pydot sphinx sphinx_rtd_theme lxml"
py_shyft_packs="pyyaml 'numpy>1.20' netcdf4  matplotlib requests pytest coverage pytest-cov pip shapely  pyproj pillow anaconda-client twine mypy"
py_cf_packs="sphinx-autodoc-typehints 'pyside2>=5.13.2' 'pint<0.20.0'"
# breaking changes in pint 0.20,
conda install --channel conda-forge mamba
mamba install --channel main --channel conda-forge ${py_build_packs} # needed for conda build and upload build steps
mamba create -n shyft_311 --channel main --channel conda-forge python=3.11 ${py_shyft_packs} ${py_dashboard_packs} ${py_cf_packs}
# needed extra LD_LIBRARY_PATH on activation since we do test python code insource, and we do not have a path to the libpython3.x.so... file
for e in shyft_311; do
    conda env config vars  set LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/projects/miniconda/envs/${e}/lib -n ${e}
done
conda clean -ay
cd /tmp

echo Building ${libfmt_name}
wget -q "https://github.com/fmtlib/fmt/archive/refs/tags/${libfmt_ver}.tar.gz"
tar -xf ${libfmt_ver}.tar.gz
pushd ${libfmt_name}
cmake -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_LIBDIR=lib -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common}
cmake --build build -j 8 --config Release --target install
popd
echo Done ${libfmt_name}


echo Building ${armadillo_name}
wget -q  http://sourceforge.net/projects/arma/files/${armadillo_name}.tar.xz
tar -xf ${armadillo_name}.tar.xz
pushd ${armadillo_name}
cmake . -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" -DDETECT_HDF5=false -DCMAKE_INSTALL_LIBDIR=lib ${cmake_common}
make install
popd
echo Done ${armadillo_name}

echo Building ${dlib_name}
dlib_archive="v${dlib_ver}.tar.gz"
if [ ! -f "${dlib_archive}" ]; then
  wget -q "https://github.com/davisking/dlib/archive/${dlib_archive}"
fi;
tar -xf "${dlib_archive}"
pushd ${dlib_name}
if [ ${dlib_ver} == "19.23" -o ${dlib_ver} == "19.24" -o ${dlib_ver} == "19.20" -o ${dlib_ver} == "19.21" -o ${dlib_ver} == "19.22" ]; then
cat <<EOF >patch_dlib_nagle.diff
312c312
<     {}
---
>     {disable_nagle();}
EOF
patch -b dlib/sockets/sockets_kernel_2.cpp patch_dlib_nagle.diff
echo "dlib socket connection patched with disable_nagle"
fi;
mkdir -p build
dlib_cfg="-DDLIB_PNG_SUPPORT=0 -DDLIB_GIF_SUPPORT=0 -DDLIB_LINK_WITH_SQLITE3=0 -DDLIB_NO_GUI_SUPPORT=1 -DDLIB_DISABLE_ASSERTS=1 -DDLIB_JPEG_SUPPORT=0 -DDLIB_USE_BLAS=0 -DDLIB_USE_LAPACK=0 -DBUILD_SHARED_LIBS=ON"
cd build && cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" -DCMAKE_INSTALL_LIBDIR=lib ${cmake_common} ${dlib_cfg} && cmake --build . --target install
popd
echo Done ${dlib_name}

echo Building doctest
git clone https://github.com/doctest/doctest
pushd doctest
cmake . -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} -DDOCTEST_WITH_TESTS=0 -DCMAKE_CXX_STANDARD=17
make install
popd
echo Done doctest

echo Building Howard Hinnant date extensions to chrono
git clone https://github.com/HowardHinnant/date
pushd date
cmake . -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
cmake --build . --target install
popd
echo Done HowardHinnant date extensions

cd  /tmp
echo Building boost_${boost_ver}
if [ ! -f boost_${boost_ver}.tar.gz ]; then
    wget -q -O boost_${boost_ver}.tar.gz https://boostorg.jfrog.io/artifactory/main/release/${boost_ver//_/.}/source/boost_${boost_ver}.tar.gz
fi;
tar -xf boost_${boost_ver}.tar.gz
pushd boost_${boost_ver}
./bootstrap.sh --prefix="${SHYFT_DEPENDENCIES_DIR}"
py_root=${SHYFT_WORKSPACE}/miniconda/envs  # here we could tweak using virtual-env, conda or system.. match with cmake
# have to help boost figure out right python versions bin,inc and libs.
# first remove any python that was found with the bootstrap step above
mv -f project-config.jam x.jam
cat x.jam | sed -e 's/using python/#using python/g' >project-config.jam
echo "# injected by shyft build/build_dependencies.sh to map explicit python versions" >>project-config.jam
echo "using python : 3.11 : ${py_root}/shyft_311/bin/python : ${py_root}/shyft_311/include/python3.11 : ${py_root}/shyft_311/lib ;" >>project-config.jam
boost_packages="--with-system --with-filesystem --with-date_time --with-python --with-serialization --with-chrono --with-thread --with-atomic --with-program_options --with-test --with-math python=3.11"
./b2 -j4 -d0 link=shared variant=release threading=multi ${boost_packages}
./b2 -j4 -d0 install link=shared variant=release threading=multi   ${boost_packages}
popd
echo  Done boost_${boost_ver}

echo Pybind11
git clone https://github.com/pybind/pybind11.git
pushd pybind11
git checkout master
git pull
git checkout ${pybind11_ver} > /dev/null
mkdir -p build
cd build && cmake -DPYTHON_EXECUTABLE=$(which python) -DCMAKE_INSTALL_PREFIX=${SHYFT_DEPENDENCIES_DIR} -DPYBIND11_TEST=0 ${cmake_common} .. && cmake -P cmake_install.cmake
popd
echo Done pybind11
cd  /tmp

echo Starting leveldb
git clone https://github.com/google/leveldb
pushd leveldb
sed -i "s/add_library(leveldb \"\")/add_library(leveldb \"SHARED\")/g" CMakeLists.txt
sed -e '/fno-rtti/d' -i CMakeLists.txt
BUILD_SHARED_LIBS=on cmake -B build -DBUILD_SHARED_LIBS=1 -DCMAKE_BUILD_TYPE=Release -DLEVELDB_BUILD_TESTS=OFF -DLEVELDB_BUILD_BENCHMARKS=OFF
cmake -B build -DLEVELDB_BUILD_TESTS=OFF -DLEVELDB_BUILD_BENCHMARKS=OFF
cmake --build build -j 6
cmake --install build --prefix ${SHYFT_DEPENDENCIES_DIR}
popd
echo Done leveldb
echo Staring rocksdb
git clone --depth 1 --branch v${rocksdb_ver} https://github.com/facebook/rocksdb
pushd rocksdb
cmake -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=${SHYFT_DEPENDENCIES_DIR} -DPORTABLE=1 -DUSE_RTTI=ON -DWITH_GFLAGS=0 -DWITH_JEMALLOC=0 -DWITH_LIBURING=1 -DWITH_CORE_TOOLS=OFF -DWITH_TOOLS=OFF -DWITH_TRACE_TOOLS=OFF -DWITH_RUNTIME_DEBUG=OFF -DWITH_BENCHMARK_TOOLS=OFF -DWITH_TESTS=OFF -DWITH_EXAMPLES=OFF -DWITH_ALL_TESTS=OFF -DWITH_JNI=OFF -DJNI=0 -DWITH_TBB=0 -DWITH_SNAPPY=1 -DWITH_ZLIB=1 -DWITH_LZ4=1 -DWITH_ZSTD=1
cmake --build build -j 6
cmake --install build --prefix ${SHYFT_DEPENDENCIES_DIR}
# cmake/rocksdb is broken on linux 8 series, so we rely on pkgconfig
rm -rf  ${SHYFT_DEPENDENCIES_DIR}/lib64/cmake/rocksdb
popd
echo Doing the otl header-only otlv4.h
wget -O otlv4_h2.zip http://otl.sourceforge.net/otlv4_h2.zip
unzip otlv4_h2.zip -d "${SHYFT_DEPENDENCIES_DIR}"/include
echo Done otlv4.h
