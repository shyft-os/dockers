#!/usr/bin/env bash
set -e
#set -x
# run as buildah unhare
build_image() {
  src_image=$1
  dest_image=$2
  build=$(buildah  from "${src_image}")
  build_mount=$(buildah mount ${build})
  src=$(readlink --canonicalize --no-newline `dirname ${0}`)
  cp ${src}/build_deps.sh ${build_mount}/tmp
  buildah run ${build} -- microdnf --enablerepo=ol8_codeready_builder install -y \
    which make ninja-build bzip2 perl nano wget tar unzip patch \
    openssl-devel lapack-devel  snappy-devel lz4-devel libzstd-devel \
    mesa-libGL-devel graphviz zstd gcc-toolset-13 git git-lfs
  echo "source /opt/rh/gcc-toolset-13/enable" >>${build_mount}/root/.bashrc
  #echo "export SHYFT_DEPENDENCIES_DIR=/projects/shyft_dependencies" >>${build_mount}/root/.bashrc
  #echo "export QT_QPA_PLATFORM=offscreen">>${build_mount}/root/.bashrc
  buildah config \
      --author sigbjorn.helset@gmail.com \
      --env QT_QPA_PLATFORM=offscreen \
      --env SHYFT_DEPENDENCIES_DIR=/projects/shyft_dependencies \
      ${build}
  mkdir -p ${build_mount}/projects/shyft_dependencies
  buildah run ${build} -- git lfs install
  buildah run ${build} -- git config --global pull.rebase false
  buildah run ${build} -- bash /tmp/build_deps.sh
  rm -rf ${build_mount}/tmp/* || true
  rm -rf ${build_mount}/var/cache/* || true
  rm -rf ${build_mount}/var/log/* || true
  buildah  commit ${build} "${dest_image}"
}

if [ $# -ne 2 ]; then
  echo "usage:buildah unshare " "$0" "src-base-image" "dest-image"
  echo "example:" "$0" "oraclelinux:8-slim conda-bbox:latest"
  (exit 1)
fi
build_image "$1" "$2"