#!/bin/bash
export CMAKE_BUILD_PARALLEL_LEVEL=4
tmpdir=$(mktemp -d)
build_pkgs1="mpfr libjpeg-turbo libpng libx11 lapack arpack openssl sudo tar unzip grep nano openssh base-devel"
build_pkgs2="gdb gcc-fortran tcsh arpack cmake ccache valgrind git git-lfs ninja"

aur_pkgs="python-bokeh"
# for some reason this one fails on wget: python-bokeh
#aur_pkgs="doctest dlib"
any_failed=0
sudo pacman --noconfirm -Sy
pushd ${tmpdir}
for pkg in ${aur_pkgs}; do
  echo "build ${pkg}"
  wget -qO - https://aur.archlinux.org/cgit/aur.git/snapshot/${pkg}.tar.gz | tar -xvz
  pushd ${pkg}
  makepkg -cf
  sudo pacman --noconfirm -U ${pkg}-*.pkg.tar.xz
  popd
done
popd
sudo rm -rf ${tmpdir}
# maybe later, if we put in some of the deps.: sudo pacman --noconfirm -R base-devel
