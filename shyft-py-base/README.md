# Docker Python Base  Image for Shyft on Arch Linux

This repository contains all scripts and files needed to create a Docker python base image for shyft on
the basis of the  Arch Linux distribution.


## Dependencies 
If you are a luck user of archlinux, 
and your are going to build (or fix) the docker your self:

Ensure you have installed the following Arch Linux packages:
 
* make
* devtools
* docker

We guess you have ~ 24..32 GB Ram and 4+ cores to work with.

### Tip

Add your user to the docker group after installing docker makes it easier to
work since you can do most docker commands without sudo.
E.g.:

`usermod -aG docker $USER`

## Usage
Run `sudo make docker-image` to build the base image

## Purpose
* Provide a base-image that is ready to receive Shyft-python builds from arch-build pipeline
* Easy deployment of python-based Shyft applications

## Use of the Shyft python base  image
The image is meant to be used as a base where we can add 
python build-artifacts from  shyft-os/shyft.

## To build the docker image

Given that you have installed the prerequisits.

To verify that you have a working setup, just try to build the 
docker 

https://hub.docker.com/r/archlinux/base

where the recipie is available here

https://github.com/archlinux/archlinux-docker

If you have a decent setup, its time to work on this docker that is a mod of the archlinux one.

* git clone shyft-os/dockers
* cd shyft-base
* sudo make docker-image
* then maybe add/update packages file, or the build_install_aur_pkgs.sh file
* test the docker image, building and testing shyft with it, e.g. as explained above

## To publish a new version

Main help/recipe is here:
 
 https://docs.gitlab.com/ee/user/packages/container_registry


Given that you are member with needed access in gitlab.com/shyft-os, 
you generate an api-token to use for the login (because we are 2FA)
Then login using the token as password, e.g.
```bash
 docker login -u <your-user@gitlab.com> --password-stdin registry.gitlab.com/shyft-os/dockers
```
* then push
```bash
sudo docker push registry.gitlab.com/shyft-os/dockers/arch-dev
```
