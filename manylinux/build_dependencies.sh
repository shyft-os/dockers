#!/bin/bash

set -o errexit # exit on failure set -e set -o nounset # exit on undeclared vars set -u
set -o pipefail # exit status of the last command that threw non-zero exit code returned
# Debugging set -x
# set -o xtrace

export SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/../..)}
build_support_dir=$(readlink --canonicalize --no-newline `dirname ${0}`)
# to align the cmake support:
SHYFT_DEPENDENCIES_DIR=${SHYFT_DEPENDENCIES_DIR:=${SHYFT_WORKSPACE}/shyft_dependencies}
armadillo_name=armadillo-10.4.1
dlib_ver=19.22
dlib_name="dlib-${dlib_ver}"
boost_ver=1_76_0
pybind11_ver=v2.4.3
miniconda_ver=latest

cmake_common="-DCMAKE_INSTALL_MESSAGE=NEVER"
echo ---------------
echo Update/build shyft dependencies
echo SHYFT_WORKSPACE........: ${SHYFT_WORKSPACE}
echo SHYFT_DEPENDENCIES_DIR.: ${SHYFT_DEPENDENCIES_DIR}
echo PACKAGES...............: miniconda ${miniconda_ver} w/shyft_env, doctest, boost_${boost_ver}, ${armadillo_name}, ${dlib_name} ,otlv4,Howard Hinnant date

# A helper function to compare versions
function version { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }

# the current versions we are building
mkdir -p "${SHYFT_DEPENDENCIES_DIR}"
cd "${SHYFT_DEPENDENCIES_DIR}"

if [ ! -d ${armadillo_name} ]; then 
    echo Building ${armadillo_name}
    if [ ! -f ${armadillo_name}.tar.xz ]; then 
        wget  http://sourceforge.net/projects/arma/files/${armadillo_name}.tar.xz
    fi;
    tar -xf ${armadillo_name}.tar.xz
    pushd ${armadillo_name}
    cmake . -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" -DDETECT_HDF5=false -DCMAKE_INSTALL_LIBDIR=lib ${cmake_common}
    make install
    popd
fi;
echo Done ${armadillo_name}

if [ ! -d ${dlib_name} ]; then
    echo Building ${dlib_name}
    dlib_archive="v${dlib_ver}.tar.gz"
    if [ ! -f "${dlib_archive}" ]; then
        wget "https://github.com/davisking/dlib/archive/${dlib_archive}"
    fi;
    tar -xf "${dlib_archive}"
    pushd ${dlib_name}
    if [ ${dlib_ver} == "19.19" -o ${dlib_ver} == "19.18" -o ${dlib_ver} == "19.20" -o ${dlib_ver} == "19.21" -o ${dlib_ver} == "19.22" ]; then
      patch -b dlib/sockets/sockets_kernel_2.cpp "${build_support_dir}/patch_dlib_nagle.diff"
      echo "dlib socket connection patched with disable_nagle"
    fi;
    mkdir -p build
    dlib_cfg="-DDLIB_PNG_SUPPORT=0 -DDLIB_GIF_SUPPORT=0 -DDLIB_LINK_WITH_SQLITE3=0 -DDLIB_NO_GUI_SUPPORT=1 -DDLIB_DISABLE_ASSERTS=1 -DDLIB_JPEG_SUPPORT=0 -DDLIB_USE_BLAS=0 -DDLIB_USE_LAPACK=0 -DBUILD_SHARED_LIBS=ON"
    cd build && cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" -DCMAKE_INSTALL_LIBDIR=lib ${cmake_common} ${dlib_cfg} && cmake --build . --target install
    popd
fi;
echo Done ${dlib_name}

if [ ! -d doctest ]; then
    echo Building doctest
    git clone https://github.com/onqtam/doctest
    pushd doctest
    cmake . -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} -DDOCTEST_WITH_TESTS=0 -DCMAKE_CXX_STANDARD=17
    make install
    popd
fi;
echo Done doctest

if [ ! -d date ]; then
    echo Building Howard Hinnant date extensions to chrono
    git clone https://github.com/HowardHinnant/date
    pushd date
    cmake . -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
    cmake --build . --target install
    popd
fi;
echo Done HowardHinnant date extensions

cd "${SHYFT_WORKSPACE}"


# Update conda to latest version, assume we start with 4.3 which
# requires PATH to be set
OLDPATH=${PATH}
export PATH="${SHYFT_WORKSPACE}/miniconda/bin:$PATH"


py_build_packs="setuptools"
py_dashboard_packs="bokeh pydot sphinx sphinx_rtd_theme lxml"
py_shyft_packs="pyyaml numpy netcdf4 cftime matplotlib requests pytest coverage pytest-cov pip shapely  pyproj pillow"
py_cf_packs="sphinx-autodoc-typehints pyside2==5.13.2 pint"
/opt/python/cp39-cp39/bin/pip install ${py_build_packs} ${py_dashboard_pacs} ${py_shyft_packs} ${py_cf_packs}


cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d boost_${boost_ver} ]; then
    echo Building boost_${boost_ver}
    if [ ! -f boost_${boost_ver}.tar.gz ]; then
        wget -O boost_${boost_ver}.tar.gz https://boostorg.jfrog.io/artifactory/main/release/${boost_ver//_/.}/source/boost_${boost_ver}.tar.gz
    fi;
    tar -xf boost_${boost_ver}.tar.gz
    pushd boost_${boost_ver}
    ./bootstrap.sh --prefix="${SHYFT_DEPENDENCIES_DIR}"
    py_root=${SHYFT_WORKSPACE}/miniconda/envs  # here we could tweak using virtual-env, conda or system.. match with cmake
    # have to help boost figure out right python versions bin,inc and libs.
    # first remove any python that was found with the bootstrap step above
    mv -f project-config.jam x.jam
    cat x.jam | sed -e 's/using python/#using python/g' >project-config.jam
    echo "# injected by shyft build/build_dependencies.sh to map explicit python versions" >>project-config.jam
    echo "using python : 3.8 : /opt/python/cp38-cp38/bin/python : /opt/python/cp38-cp38/include/python3.8 : /opt/python/cp38-cp38/lib ;" >>project-config.jam
    echo "using python : 3.9 : /opt/python/cp39-cp39/bin/python : /opt/python/cp39-cp39/include/python3.9 : /opt/python/cp39-cp39/lib ;" >>project-config.jam
    boost_packages="--with-system --with-filesystem --with-date_time --with-python --with-serialization --with-chrono --with-thread --with-atomic --with-math python=3.9,3.8"
    ./b2 -j4 -d0 link=shared variant=release threading=multi ${boost_packages}
    ./b2 -j4 -d0 install link=shared variant=release threading=multi   ${boost_packages}
    popd
fi;
echo  Done boost_${boost_ver}

cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d pybind11 ]; then
    git clone https://github.com/pybind/pybind11.git
    pushd pybind11
    git checkout master
    git pull
    git checkout ${pybind11_ver} > /dev/null
    mkdir -p build
    cd build && cmake -DPYTHON_EXECUTABLE=$(which python) -DCMAKE_INSTALL_PREFIX=${SHYFT_DEPENDENCIES_DIR} -DPYBIND11_TEST=0 ${cmake_common} .. && cmake -P cmake_install.cmake
    popd
fi;
echo Done pybind11
cd ${SHYFT_DEPENDENCIES_DIR}

echo Doing the otl header-only otlv4.h
if [ ! -f include/otlv4.h ]; then
    echo ..missing, then download and install otlv4.h
    wget -O otlv4_h2.zip http://otl.sourceforge.net/otlv4_h2.zip
    unzip otlv4_h2.zip -d include
fi;
echo Done otlv4.h



