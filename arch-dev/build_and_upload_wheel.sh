#!/bin/bash
set -o errexit
set -o pipefail

git clone https://gitlab.com/shyft-os/shyft
cd shyft && mkdir build && cd build
cmake .. -G Ninja -DCMAKE_BUILD_TYPE=Release
#cmake --build . --config Release --target install
ninja install
cd ../python && python setup.py bdist_wheel

TWINE_PASSWORD=${GITLAB_PYPI_TOKEN} TWINE_USERNAME=${GITLAB_PYPI_USER} python3 -m twine upload --repository-url https://gitlab.com/api/v4/projects/15783810/packages/pypi dist/*



