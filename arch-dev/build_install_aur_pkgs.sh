#!/bin/bash
set -o errexit
set -o pipefail
export CMAKE_BUILD_PARALLEL_LEVEL=8
tmpdir=$(mktemp -d)

aur_pkgs="python-xyzservices python-bokeh armadillo dlib"
# for some reason this one fails on wget: python-bokeh
#aur_pkgs="doctest dlib"
any_failed=0
sudo pacman --noconfirm -Sy
pushd ${tmpdir}
for pkg in ${aur_pkgs}; do
  echo "build ${pkg}"
  wget -qO - https://aur.archlinux.org/cgit/aur.git/snapshot/${pkg}.tar.gz | tar -xvz
  pushd ${pkg}
  makepkg -cf
  sudo pacman --noconfirm -U ${pkg}-*.pkg.tar.*
  popd
done

curl https://gitlab.com/jehelset/cpack-makepkg/-/package_files/22406673/download > cpack-makepkg.pkg.tar.zst
sudo pacman -U cpack-makepkg.pkg.tar.zst --noconfirm

# build rocksdb in a way that avoids problem with the jemalloc and py extensions.
git clone -b v8.10.0 --depth 1 --single-branch https://github.com/facebook/rocksdb
pushd rocksdb
cmake -B build -G Ninja -DCMAKE_BUILD_TYPE=Release -DPORTABLE=1 -DUSE_RTTI=ON -DWITH_GFLAGS=0 -DWITH_JEMALLOC=0 -DWITH_LIBURING=1 -DWITH_CORE_TOOLS=OFF -DWITH_TOOLS=OFF -DWITH_TRACE_TOOLS=OFF -DWITH_RUNTIME_DEBUG=OFF -DWITH_BENCHMARK_TOOLS=OFF -DWITH_TESTS=OFF -DWITH_EXAMPLES=OFF -DWITH_ALL_TESTS=OFF -DWITH_JNI=OFF -DJNI=0 -DWITH_TBB=1 -DWITH_SNAPPY=1 -DWITH_ZLIB=1 -DWITH_LZ4=1 -DWITH_ZSTD=1
cmake --build build
sudo cmake --install build --prefix /usr
popd

popd
echo "installing @angular/cli at system level"
echo n | sudo npm install -g --silent @angular/cli
sudo rm -rf ${tmpdir}
