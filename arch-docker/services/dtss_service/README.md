# Dtss service image

## Running image
Running the dtss_service image spawns a container, listening internally on 20000 
DtsClient requests, and on 20001 on web-api requests.

One can start the image by
```shell
docker run registry.gitlab.com/shyft-os/dockers/dtss_service
```
Or using docker-compose.
```shell
docker-compose -d up
```
Using docker-compose is advised as it also takes care of port and volume mapping for you.
Just change the provided .env file to suit your needs, and docker-compose will use the defined variables
from .env.

## Configuration
```shell
# Distributed TimeSeries Server
DTSS_SERVICE_ROOT=/srv/shyft_root/dtss
DTSS_HOST=127.0.0.1
DTSS_PORT=20000
DTSS_WEB_HOST=127.0.0.1
DTSS_WEB_PORT=20001
DTSS_CONTAINERS=sensor,forecast
DTSS_WEB_API_ROOT=/srv/shyft_root/www
DTSS_MEM_TARGET=5000000000 # 5GB memory target on cache
LOG_INTERVAL=60
```

# Example, collecting data from FROST API
```text
The Frost API provides free access to MET Norway's archive of historical weather and climate data.
This data includes quality controlled daily, monthly, and yearly measurements of temperature, precipitation, 
and wind data. Other information, like metadata about weather stations, is also available through the API. 
```

## Prerequisite
### Configuration 
Create a directory and copy the docker-compose.yml and create a .env file with the following content
```shell
# Distributed TimeSeries Server
DTSS_SERVICE_ROOT=/srv/shyft_root/dtss  # Or any other directory of your choice
DTSS_HOST=127.0.0.1
DTSS_PORT=5000
DTSS_WEB_HOST=127.0.0.1
DTSS_WEB_PORT=6000
DTSS_CONTAINERS=sensor,forecast
DTSS_WEB_API_ROOT=/srv/shyft_root/www
DTSS_MEM_TARGET=5000000000 # 5GB memory target on cache
LOG_INTERVAL=60
```

Open a terminal and run 
```shell
docker-compose up -d   (skip -d if you want to see live output)
```

Using an altered version of FROST API example https://frost.met.no/python_example.html
```python
import requests
import shyft.time_series as sa

# Insert your own client ID here
## NOTE not needed for this example
client_id = ''

# Define endpoint and parameters
endpoint = 'https://in2000-frostproxy.ifi.uio.no/observations/v0.jsonld'
sources = "SN18700"
elements = "air_temperature"
parameters = {
    'sources': f'{sources}',
    'elements': f'{elements}',
    'referencetime': '2021-04-01/2021-04-04',
}
# Issue an HTTP GET request
r = requests.get(endpoint, parameters, auth=(client_id,''))
# Extract JSON data
json = r.json()

# Check if the request worked, print out any errors
if r.status_code == 200:
    data = json['data']
    print('Data retrieved from frost.met.no!')
else:
    print('Error! Returned status code %s' % r.status_code)
    print('Message: %s' % json['error']['message'])
    print('Reason: %s' % json['error']['reason'])

```
Wash out sensor data from json response
```python
temperature = []
quality = []
time_points = []
for d in data:
    source_id = d["sourceId"]
    ref_time = sa.time(d["referenceTime"])
    seconds = ref_time.seconds
    element_id = d["observations"][0]["elementId"]
    value = d["observations"][0]["value"]
    measurement_quality = d["observations"][0]["qualityCode"]
    temperature.append(value)
    quality.append(measurement_quality)
    time_points.append(seconds

```
Create shyft timeaxis and timeseries, and store to DtsServer running in dtss_service container
```python
time_points.append(time_points[-1] + cal.HOUR)
time_axis = sa.TimeAxis(time_points)
temperature_ts = sa.TimeSeries(time_axis, temperature, sa.POINT_AVERAGE_VALUE)
temperature_ts = sa.TimeSeries(f"shyft://source/{sources}/temperature", temperature_ts)

qualtiy_ts = sa.TimeSeries(time_axis, quality, sa.POINT_AVERAGE_VALUE)
qualtiy_ts = sa.TimeSeries(f"shyft://source/{sources}/measurement_quality", qualtiy_ts)

tsv = sa.TsVector()
tsv.extend([temperature_ts, qualtiy_ts])
dtsc = sa.DtsClient("127.0.0.1:5000")
dtsc.store_ts(tsv)
```

Inspecting your DTSS_SERVICE_ROOT directory defined in .env, you should see the following structure

```shell
├── dtss
│   ├── dtss_log.txt
│   ├── forecast
│   ├── source
│   │   └── SN18700
│   │       ├── measurement_quality
│   │       └── temperature
│   ├── ts
│   └── www
└── www

```

Query data from the dtss_service container

```python
cal = sa.Calendar("Europe/Oslo")
# The requested period we used in FROST API 2021-04-01/2021-04-04',
t_start = cal.time(2021, 4, 1)
ta = sa.TimeAxis(t_start, cal.DAY, 2) # We don't include the last day in period used to query
ts_read_temp = sa.TimeSeries(f"shyft://source/{sources}/temperature")
tsv_read = sa.TsVector()
tsv_read.append(ts_read_temp)

tsv_response = dtsc.evaluate(tsv_read, ta.total_period())
assert len(tsv_response) == 1
ts_tmp = tsv_response[0]
print(ts_tmp(cal.time(2021, 4, 2, 10))) # 6.9
print(ts_tmp(cal.time(2021, 4, 3)))     # 1.0
print(ts_tmp(cal.time(2021, 4, 3, 1)))  # Nan, since we are asking 1h outside period
```
