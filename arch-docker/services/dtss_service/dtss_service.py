import os
import signal
import logging
from os import environ
from time import sleep
from enum import Enum
from pathlib import Path
from shyft.time_series import DtsServer, utctime_now, time


class Configuration(Enum):
    """Enum for defining which ports are used the services

    Notes:
        Containers can be defined by a comma separated string.
        DTSS = "test,foo" will create support for defining shyft://test and shyft://foo
       when creating shyft timeseries.
    """

    INTERFACE = "0.0.0.0"  # inside a docker, we listen for 0.0.0.0, then we map this to external netw.
    PORT = 20000
    WEB_INTERFACE = "0.0.0.0"
    PORT_WEB_API = 20001
    WEB_ROOT = environ.get("DTSS_WEB_ROOT", "/srv/shyft_root/www")

    CONTAINER_ROOT = environ.get("DTSS_ROOT", "/srv/shyft_root/dtss")
    CONTAINERS = environ.get("DTSS_CONTAINERS", "ts")

    MEMORY_TARGET = int(environ.get("DTSS_MEM_TARGET", 5 * 1000 * 1000 * 1000))

    LOG_INTERVAL = int(environ.get("LOG_INTERVAL", 60))


class Exit:
    """ A small helper-class to raise a flag if SIGTERM is received """
    now: bool = False  # signal handler sets this to True, and poll loop above terminates in controlled fashion

    def __init__(self):
        signal.signal(signal.SIGTERM, self.exit)

    @staticmethod
    def exit(signum, frame):
        Exit.now = True


def configure_logger(filename: Path = None, filemode: str = "w", level: int = logging.DEBUG,
                     log_format: str = '[%(asctime)s] %(name)-12s %(levelname)-8s %(message)s',
                     datefmt: str = '%m-%d %H:%M:%S'):

    for h in logging.root.handlers[:]:
        logging.root.removeHandler(h)
    logging.basicConfig(level=level,
                        format=log_format,
                        datefmt=datefmt,
                        filename=filename.as_posix(),
                        filemode=filemode)
    console = logging.StreamHandler()
    console.setLevel(level)
    formatter = logging.Formatter('[%(asctime)s] %(levelname)-8s: %(name)-12s %(message)s',
                                  datefmt='%m-%d %H:%M:%S')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


class DtssService:

    def __init__(self):

        self.dtss = DtsServer()
        self.container_root = Path(Configuration.CONTAINER_ROOT.value)
        if not self.container_root.exists():
            self.container_root.mkdir()

        configure_logger(self.container_root / Path("dtss_log.txt"))
        self.log = logging.getLogger('')

        self.dtss.set_listening_port(20000)
        self.dtss.cache_memory_target = Configuration.MEMORY_TARGET.value
        self.set_containers(Configuration.CONTAINERS.value)

    def process_container_list(self, container_list: str):
        processed_list = list()
        con_list = container_list.split(",")
        for con in con_list:
            processed_list.append(self.container_root / Path(f"{con}"))
        return processed_list

    def set_containers(self, container_list: str):
        processed_list = self.process_container_list(container_list)
        for con_name in processed_list:
            assert isinstance(con_name, Path), "container_map value should be Path"
            if not con_name.is_dir() or not con_name.is_symlink():
                con_name.mkdir(parents=True, exist_ok=True)
            self.log.info(f"Setting container {con_name.name} -> {con_name.as_posix()}")
            self.dtss.set_container(con_name.name, con_name.as_posix())

    def start_service(self):

        self.log.info(f"dtss-service: starting on {Configuration.INTERFACE.value}:{Configuration.PORT.value}")
        self.dtss.start_async()
        self.log.info(
            f"dtss-service: starting web-api on {Configuration.INTERFACE.value}:{Configuration.PORT_WEB_API.value} serving {Configuration.WEB_ROOT.value}")
        self.dtss.start_web_api(host_ip=Configuration.WEB_INTERFACE.value,
                                port=Configuration.PORT_WEB_API.value,
                                doc_root=Configuration.WEB_ROOT.value,
                                fg_threads=4,
                                bg_threads=4)

        ex = Exit()
        t_report = utctime_now()
        report_interval = Configuration.LOG_INTERVAL.value
        while True:
            sleep(1)
            if ex.now:
                break
            if utctime_now() - t_report > report_interval:
                srv_status = self.dtss.is_running()
                self.log.info(f"dtss-service: Running:{srv_status} \n"
                              f"hits: {self.dtss.cache_stats.hits} "
                              f"misses: {self.dtss.cache_stats.misses} "
                              f"id_count: {self.dtss.cache_stats.id_count}")
                t_report = utctime_now()

        self.log.info(f'terminating dtss service ...')
        self.log.info(f'terminate dtss sercice@{self.dtss.get_listening_port()} ')
        self.stop_service()

    def stop_service(self):
        self.dtss.stop_web_api()


if __name__ == "__main__":
    dtss_service = DtssService()
    dtss_service.start_service()
