# Docker shyft-base with binary dependencies Arch Linux

This docker contains the bare minimum of binaries/libraries
needed to run a compiled Shyft binary.
It is used as base image for shyft-py-base, that also contains
the python 3rd party dependencies of shyft.
 
## Dependencies 
If you are a luck user of archlinux, 
and your are going to build (or fix) the docker your self:

Ensure you have installed the following Arch Linux packages:
 
* make
* devtools
* docker

We guess you have ~ 24..32 GB Ram and 4+ cores to work with.

### Tip

Add your user to the docker group after installing docker makes it easier to
work since you can do most docker commands without sudo.
E.g.:

`usermod -aG docker $USER`

## Usage
Run `sudo make docker-image` to build the base image

## Purpose
* Provide absolute minimal docker with dependencies, like boost etc.
* Provide the most simple but complete image to base every other upon
* Used in docker build-pipeline at the end of shyft-builds

## Use of the Shyft developer image
The image contains all recent c++ compilers, debuggers, and complete set of python modules.
SHYFT_DEPENDENCIES_DIR is unset, but all packages are found
on the system.

## aur packages

* boost 
* armadillo 
* superlu (required by armadillo in this setup)
* dlib
* refer to build_install_aur_pkgs.sh script for complete and full details

## Example usage

```bash
sudo docker run -it -u ci -v $HOME/projects:/home/ci/projects registry.gitlab.com/shyft-os/dockers/shyft-base:latest bash
```

## To build the docker image

Given that you have installed the prerequisits.

To verify that you have a working setup, just try to build the 
docker 

https://hub.docker.com/r/archlinux/base

where the recipe is available here

https://github.com/archlinux/archlinux-docker

If you have a decent setup, its time to work on this docker that is a mod of the archlinux one.

* git clone shyft-os/dockers
* cd shyft-base
* sudo make docker-image
* then maybe add/update packages file, or the build_install_aur_pkgs.sh file
* test the docker image, building and testing shyft with it, e.g. as explained above

## To publish a new version

Main help/recipe is here:
 
 https://docs.gitlab.com/ee/user/packages/container_registry


Given that you are member with needed access in gitlab.com/shyft-os, 
you generate an api-token to use for the login (because we are 2FA)
Then login using the token as password, e.g.
```bash
 docker login -u <your-user@gitlab.com> --password-stdin registry.gitlab.com/shyft-os/dockers
```
* then push
```bash
sudo docker push registry.gitlab.com/shyft-os/dockers/shyft-base
```
